import string
import random


class BarcodeScanner:
    @staticmethod
    def scan_code() -> str:
        return "plate-" + "".join(random.choice(string.digits) for _ in range(8))
