import time
import webbrowser

from sila2.client import SilaClient
import warnings
warnings.simplefilter("ignore", UserWarning)

from barcode_scanner import BarcodeScanner

# *** setup ***
barcode_scanner = BarcodeScanner()
erp_system = SilaClient("biosash1.splashlake.com", 50056)
plate_reader = SilaClient("biosash1.splashlake.com", 50054, insecure=True)
result_deposit = SilaClient("biosash1.splashlake.com", 50055)

# *** execution ***

# scan sample barcode of microplate
plate_id = barcode_scanner.scan_code()

# get analytical method for the scanned microplate from ERP system
method, wells = erp_system.WorklistService.GetTasksForPlate(plate_id)

# run microplate reader with the appropriate method
read_instance = plate_reader.MicroplateReaderService.Read(method, plate_id, wells)
# wait for reader to complete
while not read_instance.done:
    time.sleep(0.1)
# fetch plate reader result in AnIML format
animl_xml, = read_instance.get_responses()

# send result to AnIML viewer
viewer_url, = result_deposit.ResultDepositService.DepositResult(plate_id, "lot-1", animl_xml)

# open AnIML viewer in browser
webbrowser.open_new_tab(viewer_url)
